
local composer = require( "composer" )
local widget = require( "widget" )
local json = require( "json" )

local scene = composer.newScene()
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local imagesNameTable = {}

--��������  ������������� �����
local function isFileExist( fname, path )
    local results = false
    local filePath = system.pathForFile( fname, path )
 
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" )

        if not file then
            --print( "File error: " .. errorString )
        else
            -- File exists!
            --print( "File found: " .. fname )
            results = true
            file:close()
        end
    end
 
    return results
end

local function networkListener( event )
    if ( event.isError ) then
        print( "Network error - download failed: ", event.response )
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        --print( "Displaying response image file" )
		--myImage = display.newImage( event.response.filename, event.response.baseDirectory, 60, 40 )
		table.insert(imagesNameTable, event.response.filename)
	end
end

--������ json � ��������� ��������
local function decodeJson()
	local filename = system.pathForFile( "images.json", system.ResourceDirectory )
	local decoded, pos, msg = json.decodeFile( filename )
	
	if not decoded then
		print( "Decode failed at "..tostring(pos)..": "..tostring(msg) )
	else
		for key,value in ipairs(decoded.images) 
		do
			--value �������� url ��������
			local fileName = value:match( "([^/]+)$" )
			if (isFileExist(fileName, system.DocumentsDirectory) ) then
				table.insert(imagesNameTable, fileName)
			else
				--��������� ��������
				local networkParams = {}
				network.download(
					value,
					"GET",
					networkListener,
					networkParams,
					fileName
				)
			end
		end
	end
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

	local scrollView = widget.newScrollView(
		{	
			top = 0,
			left = 0,
			horizontalScrollDisabled = true

		}
	)

	decodeJson()
	local offsetY = 25
	for i=1, #imagesNameTable, 1 
	do
		local image = display.newImageRect(imagesNameTable[i], system.DocumentsDirectory, 300, 225)
		image.x = display.contentCenterX
		image.y = image.height * 0.5 + offsetY
		offsetY = offsetY + 225
		scrollView:insert( image )
	end
	
	
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
